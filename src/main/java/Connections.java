import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Класс для установки соединения с бд
 */


public class Connections {
    public static Connection connect() throws IOException {
        String fileConfig = "config.properties";
        Properties prop = Config.prop(fileConfig);
        Connection connection;
        try {
            Class.forName(prop.getProperty("dbdriver"));
        } catch (ClassNotFoundException e) {
            System.out.println("Jdbc-driver not found");
            return null;
        }
        try {
            connection = DriverManager.getConnection(prop.getProperty("dburl"),
                    prop.getProperty("dbusername"), prop.getProperty("dbpassword"));
        } catch (SQLException e) {
            System.out.println("Connection failed");
            return null;
        }
        return connection;
    }
}

