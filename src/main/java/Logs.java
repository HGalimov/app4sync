import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Halim on 21.02.2017.
 * Класс для записи логов
 */
public class Logs {
    static Properties logProp;

    static {
        try (InputStream inputStream = Logs.class.getResourceAsStream("log4j.properties")) {
            logProp = new Properties();
            logProp.load(inputStream);
        } catch (IOException e) {

        }
    }

    public static void log(String msg) throws IOException {
        String fileConfig = "config.properties";
        Properties prop = Config.prop(fileConfig);
        String logPath = prop.getProperty("logFilePath").trim();
        if (logPath != null && logPath.length() != 0) {
            logProp.setProperty("log4j.appender.file.File", logPath);
        }
        PropertyConfigurator.configure(logProp);
        Logger logger = Logger.getLogger(Commands.class);
        logger.trace(msg);
    }
}
