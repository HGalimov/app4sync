import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by Halim on 20.02.2017.
 * Класс для парсинга xml-файла
 */
public class Parser {
    public Set setDeps(String sourcePath) {
        Set<Department> setDeps = new HashSet<>();
        try (InputStream input = new FileInputStream(sourcePath)) {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(input);
            doc.getDocumentElement().normalize();
            NodeList list = doc.getElementsByTagName("dep");

            for (int i = 0; i < list.getLength(); i++) {
                Node node = list.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    Department dep = new Department();
                    dep.setCode(getElementValue("code", element));
                    dep.setJob(getElementValue("job", element));
                    dep.setDescription(getElementValue("description", element));
                    if (setDeps.contains(dep)) {
                        return null;
                    }
                    setDeps.add(dep);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return setDeps;
    }

    public void writer(List<Department> listDeps, String destinationPath) throws IOException {
        try{

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("departments");
            doc.appendChild(rootElement);


            for(Department department: listDeps) {
                Element dep = doc.createElement("dep");
                rootElement.appendChild(dep);

                Element code = doc.createElement("code");
                code.appendChild(doc.createTextNode(department.getCode()));
                dep.appendChild(code);

                Element job = doc.createElement("job");
                job.appendChild(doc.createTextNode(department.getJob()));
                dep.appendChild(job);

                Element description = doc.createElement("description");
                description.appendChild(doc.createTextNode(department.getDescription()));
                dep.appendChild(description);
            }

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File(destinationPath));

            transformer.transform(source, result);

            System.out.println("File saved");
            Logs.log("Данные выгружены");

        }
        catch (Exception e) {
            System.out.println("File did not saved");
            Logs.log("Данные не выгружены");
        }
    }

    private String getElementValue(String tag, Element element){
        return element.getElementsByTagName
                (tag).item(0).getTextContent();
    }
}
