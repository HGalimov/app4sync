import java.io.IOException;
import java.sql.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;
public class Main {
    public static void main(String[] args) throws IOException, ParserConfigurationException, SQLException, SAXException {
       try {
           if (args[0].equals("sync")) {
               Connection connection = Connections.connect();
               Commands.sync(args[1], connection);
               return;
           }
           if (args[0].equals("pull")) {
               Connection connection = Connections.connect();
               Commands.pull(args[1], connection);
               return;
           }
           System.out.println("Error");
           return;
       }
       catch (Exception e) {
           System.out.println("Error");
           e.printStackTrace();
       }
    }
}


