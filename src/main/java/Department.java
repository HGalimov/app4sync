/**
 * Класс отдела
 */
public class Department {
    private int id;
    private String code;
    private String job;
    private String description;
    private String codeJob;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        codeJob = code + job;
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department dep = (Department) o;
        return codeJob.equals(dep.codeJob);
    }

    @Override
    public int hashCode() {
        codeJob = code + job;
        return codeJob.hashCode();
    }
}
