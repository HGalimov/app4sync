import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

/**
 * Created by Halim on 20.02.2017.
 * Класс с методами синхронизации и выгрузки
 */
public class Commands {
    private static String err = "Error";
    private static String notErr = "Success";

    public static void sync(String sourcePath, Connection connection) throws ParserConfigurationException,
            IOException, SAXException, SQLException {
        Parser parser = new Parser();
        ResultSet rs;
        int maxId = 0;
        Set<Department> setDeps = parser.setDeps(sourcePath);
        HashMap<Department, String> records = new HashMap<>();
        if (setDeps == null) {
            System.out.println(err);
            Logs.log("В исходном файле есть две или более одинаковых записи пары код отдела и название должности");
        } else {
            connection.setAutoCommit(false);
            try (PreparedStatement stmt1 = connection.prepareStatement("SELECT * FROM" +
                    " department")) {
                rs = stmt1.executeQuery();
                while (rs.next()) {
                    Department department = new Department();
                    department.setId(rs.getInt(1));
                    department.setCode(rs.getString(2));
                    department.setJob(rs.getString(3));
                    department.setDescription(rs.getString(4));
                    records.put(department, department.getDescription());
                    maxId = department.getId();
                }


                for (Department dep : setDeps) {
                    if (records.containsKey(dep)) {
                        if (!records.get(dep).equals(dep.getDescription())) {
                            try (PreparedStatement stmt2 = connection.prepareStatement("UPDATE department " +
                                    "SET description=" +
                                    "? WHERE dep_code=? and dep_job=?")) {
                                stmt2.setString(1, dep.getDescription());
                                stmt2.setString(2, dep.getCode());
                                stmt2.setString(3, dep.getJob());
                                stmt2.executeUpdate();
                            } catch (SQLException e) {
                                System.out.println(err);
                                e.printStackTrace();
                                Logs.log("Ошибка во время изменения данных");
                                connection.rollback();
                            }
                        }
                    } else
                        try (PreparedStatement stmt3 = connection.prepareStatement("INSERT INTO department(" +
                                "Id, Dep_Code, Dep_Job, Description)" +
                                "VALUES (?, ?, ?, ?)")) {
                            stmt3.setInt(1, ++maxId);
                            stmt3.setString(2, dep.getCode());
                            stmt3.setString(3, dep.getJob());
                            stmt3.setString(4, dep.getDescription());
                            stmt3.executeUpdate();
                        } catch (SQLException e) {
                            System.out.println(err);
                            e.printStackTrace();
                            Logs.log("Ошибка во время добавления данных");
                            connection.rollback();
                        }

                }
                for (Department department : records.keySet()) {
                    if (!setDeps.contains(department)) {
                        try (PreparedStatement stmt4 = connection.prepareStatement("DELETE FROM department " +
                                "WHERE id=?")) {
                            stmt4.setInt(1, department.getId());
                            stmt4.executeUpdate();
                        } catch (SQLException e) {
                            System.out.println(err);
                            e.printStackTrace();
                            Logs.log("Ошибка во время удаления данных");
                            connection.rollback();
                        }
                    }
                }

                connection.commit();
                System.out.println(notErr);
                Logs.log("Данные синхронизированы");
            } catch (SQLException e) {
                System.out.println(err);
                e.printStackTrace();
                Logs.log("Ошибка во время синхронизации");
                connection.rollback();
            }
        }
    }

    public static void pull(String destinationPath, Connection connection) throws SQLException, IOException {
        List<Department> listDeps = new ArrayList<>();
        ResultSet rs;
        Parser parser = new Parser();
        connection.setAutoCommit(false);
        try (PreparedStatement stmt1 = connection.prepareStatement("SELECT * FROM" +
                " department")) {
            rs = stmt1.executeQuery();
            while (rs.next()) {
                Department department = new Department();
                department.setCode(rs.getString(2));
                department.setJob(rs.getString(3));
                department.setDescription(rs.getString(4));
                listDeps.add(department);
            }
            connection.commit();
        } catch (SQLException e) {
            Logs.log("Ошибка во время выгрузки данных");
            connection.rollback();
            e.printStackTrace();
            System.out.println(err);
        }
        parser.writer(listDeps, destinationPath);
    }
}
