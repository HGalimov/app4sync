import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Класс для работы с файлами
 */

public class Config {
    public static Properties prop(String fileName) throws IOException {
        Properties prop = new Properties();
        fileName = currentDirApp() + fileName;
        try (InputStream input = new FileInputStream(fileName)){
            prop.load(input);
        }
        catch (IOException e){

        }
        return prop;
    }
    public static String currentDirApp() {
        //путь к jar-файлу
        String pathApp = Main.class.getProtectionDomain().getCodeSource()
                .getLocation().toString();
        String dir = pathApp.substring(pathApp.indexOf("/") + 1,
                pathApp.lastIndexOf("/") + 1);
        return dir;
    }
}
